package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    public static void main(String[] args) {
        //[Section] Java Collection
        // are single unit of objects
        // data with relevant and connected values

        //In java, arrays are container of values of the same type given a predefined amount of values.
        // java arrays are more rigid, once the size and data type are defined, they can no longer be changed.

        //Syntax: Array Declaration
            // dataType[] identifier = new dataType[numOfElements];

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;

        //Will result error -> out of bounds
        //intArray[5] = 100;

        //This will return memory address
        System.out.println(intArray);
        System.out.println(Arrays.toString(intArray));

        // String Array
        // Syntax: Array Declaration with Initialization
            //dataType identifier = {values}

        String[] names = {"John", "Jane", "Joe"};
        System.out.println(Arrays.toString(names));

        // Sample Java Array Method

        //Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort() method" + Arrays.toString(intArray));

        String[][] classroom = new String[3][3];

        //first row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.deepToString(classroom));

        //[Section] ArrayList
        //are resizable arrays, wherein elements can be added or removed
        //Syntax: ArrayList
        //ArrayList<T> identifier = new ArrayLsit<T>();

        //Declaring ArrayList
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

        //Add elements
        students.add("John");
        students.add("Paul");
        students.add("Eric");
        System.out.println(students);
        //Getting an element
        System.out.println(students.get(1));

        //Adding element in a specific
        students.add(1, "Joey");
        System.out.println(students);

        //Update element in a specific index
        students.set(0, "George");
        System.out.println(students);

        //remove element in a specific index
        students.remove(1);
        System.out.println(students);

        //remove all element
        students.clear();
        System.out.println(students);

        //get arrayList size
        System.out.println(students.size());

        //[Section] Hashmaps
        //Syntax: HashMap
        //HashMap<T, T> identifier = new HashMap<T, T>();

        HashMap<String, Integer> job_position = new HashMap<String, Integer>();
        //adding element to a HashMap
        job_position.put("Brandon", 5);
        job_position.put("Alice", 3);

        System.out.println(job_position);
        System.out.println(job_position.get("Brandon"));
        System.out.println(job_position.values());
        job_position.clear();
        System.out.println(job_position);






    }
}

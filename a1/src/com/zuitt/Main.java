package com.zuitt;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        int[] primeNumbers = new int[5];
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;
        System.out.println("The first prime number is: "+ primeNumbers[0]);

        ArrayList<String> names = new ArrayList<String>();
        names.add("John");
        names.add("Jane");
        names.add("Chloe");
        names.add("Zoey");
        System.out.println("My friends are: " + names);

        HashMap<String, Integer> products = new HashMap<String, Integer>();
        products.put("toothpaste", 15);
        products.put("toothbrush", 20);
        products.put("soap", 12);
        System.out.println("Our current inventory consists of: " + products);


    }
}
